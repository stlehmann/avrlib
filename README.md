# avrlib
This library is released under the GNU General Public License.

This is a collection of useful *AVR* libraries. The contained libraries are
listed below.

# Libraries 

## ds18x20

Library for the onewire temperature sensor DS18x20. Originally written
by Martin Thomas <eversmith@heizung-thomas.de>  

## encoder

read values of a rotary encoder with push button

## i2clcd_pcf8574x

Library for accessing a HD44780-based character LCD module via
an PCD8547X I2C port expander with 8 IO pins. Written by Nico Eichelmann 
and Thomas Eichelmann


## i2clcd_mcp23017

Slightly modified version of i2clcd_pcf8574x for accessing a HD44780-based
character LCD module via a MCP23017 I2C port expander with 16 IO pins. 

## lcd

Library for accessing a HD44780-based character LCD module. Written by
*Peter Fleury* <pfleury@gmx.de> (http://jump.to/fleury).

## my_util

Often used utility functions for *AVR* programming.

## newdelete

Implement the convenience function *new* and *delete*.

## onewire

Library for onewire support on one or multiple pins.

## pid

Implements PID controller algorithm.

## twimaster

Library for using an *AVR* as *TWI* master. Written by *Peter Fleury* 
<pfleury@gmx.ch> (http://jump.to/fleury).
API compatible with I2C Software Library *i2cmaster.h*

## twislave

Library for using an *AVR* as *TWI* slave. Writte by Martin Junghans 
<jtronics@gmx.de>. GNU General Public License

## uart

Library for UART output, input and logging.