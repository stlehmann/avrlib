i2clcd - LCD over I2C library
	Designed for HD44870 based LCDs with I2C expander PCF8574X
	on Atmels AVR MCUs

Copyright (C) 2006 Nico Eichelmann and Thomas Eichelmann

Contact	info@computerheld.de

Version 0.1
 
Files: i2clcd.h i2clcd.c

Requires I2C-Library from Peter Fleury http://jump.to/fleury

Only testet with the following configuration:
	2x16 Display (KS0070B), PCF8574P, ATMega32 @ 16 Mhz
	avr-gcc (GCC) 4.1.0

PIN-Assignment:

 PCF8574	<->		LCD
 ----------------------------------------------
 P0		<->		DB4
 P1		<->		DB5
 P2		<->		DB6
 P3		<->		DB7
 P4		<->		RS
 P5		<->		R/W
 P6		<->		-
 P7		<->		Enable
 
Example:

 #include "i2clcd.h"
 	
 int main(void)
 {
	...
	lcd_init();						//-	Display initialization
	...
	char string[] = "Hi World";
	lcd_print(string);					//-	Print a string
	lcd_gotolr(2,4);					//-	Move to position (line 2, row 4)
	
	//-	Turn cursor off and activate blinking
	lcd_command(LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKINGON);
	...
 }

For further informations and a detailed documentation visit: http://www.computerheld.de/i2clcd/index.html .
