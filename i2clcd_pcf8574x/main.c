#ifndef F_CPU
	#define F_CPU 16000000
#endif

#include "i2clcd.h"

int main(void)
{
	i2c_init();
	lcd_init();
	unsigned char string[] = "Hi World";
	lcd_print(string);				//-	Print a string
	lcd_nextline();
	
	/*
	unsigned char line = 0x00, row = 0x00;
	lcd_getlr(&line, &row);				//-	Get cursor position into two unsigned chars
	*/
	
	//-	Turn cursor off and activate blinking
	lcd_command(LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKINGON);
	
	while (true){}					//-	Endless loop
}
