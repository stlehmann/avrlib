/*
 * newdelete.cpp
 *
 * Supply the convenience functions new and delete
 *
 * Created: 26.06.2014 13:19:24
 *  Author: Lehmann
 *
 */ 

#include "newdelete.h"

void *operator new(size_t size)
{
	return malloc(size);
}

void *operator new[](size_t size)
{
	return malloc(size);
}

void operator delete(void* ptr)
{
	free(ptr);
}

void operator delete[](void* ptr)
{
	free(ptr);
}