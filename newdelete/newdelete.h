/*
 * newdelete.h
 *
 * Supply the convenience functions new and delete
 *
 * Created: 26.06.2014 13:19:33
 *  Author: Lehmann
 */ 


#ifndef NEWDELETE_H_
#define NEWDELETE_H_

#include <stdlib.h>

void *operator new(size_t size);
void *operator new[](size_t size);
void operator delete(void* ptr);
void operator delete[](void* ptr);

#endif /* NEWDELETE_H_ */