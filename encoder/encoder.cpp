/************************************************************************/
/*                                                                      */
/*            Drehgeber mit wackeligem Rastpunkt dekodieren             */
/*            Quelle: http://www.mikrocontroller.net/articles/Drehgeber */
/*			  Angepasst auf ATMEGA8										*/
/************************************************************************/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include "Encoder.h"
#include "uart.h"

/************************************************************************/
/* Debounce cycles wie folgt berechnen:                                 */
/* N = MS / F_CPU / PRESCALER / 256										*/
/* mit MS ... Entprellzeit in ms										*/
/************************************************************************/

#define PHASE_A					(PIND & (1<<PD2))			// an Pinbelegung anpassen
#define PHASE_B					(PIND & (1<<PD3))			// an Pinbelegung anpassen
#define BUTTONPRESSED			!(PIND & (1<<PD4))			// Button Pinbelegung, negiert, da negativschaltend
#define BUTTON_DEBOUNCE_CYCLES		30						// Zeit �ber die entprellt werden soll in ms
#define BUTTON_PRESSEDLONG_CYCLES	1000					// Zeitraum f�r das Erkennen eines lang gedr�ckten Tasters
#define PRESCALER 64										// Verwendeter Vorteiler, bei �nderung Register TCRR0 entsprechend �ndern

// Drehgeberbewegung zwischen zwei Auslesungen im Hauptprogramm
volatile int8_t enc_delta;
// Dekodertabelle f�r wackeligen Rastpunkt halbe Aufl�sung
const int8_t table[16] PROGMEM = {0,0,1,0,0,0,0,-1,0,0,0,0,1,0,0};    
// Dekodertabelle f�r normale Drehgeber, volle Aufl�sung
//int8_t table[16] PROGMEM = {0,1,-1,0,-1,0,0,1,1,0,0,-1,0,-1,1,0};    
	
volatile int16_t iButtonPressedCounter = 0;

typedef enum EButtonState
{
	ButtonState_Unpressed,
	ButtonState_Pressed,
	ButtonState_Hold,
	ButtonState_Released	
};	
volatile EButtonState buttonState = ButtonState_Unpressed;
volatile EButtonPressedState buttonPressed = ButtonPressed_Unpressed;
 
ISR( TIMER0_OVF_vect )             // 2ms fuer manuelle Eingabe
{
	//Encoder-Auswertung
    static int8_t last=0;           // alten Wert speichern
 
    last = (last << 2)  & 0x0F;
    if (PHASE_A) last |=2;
    if (PHASE_B) last |=1;
    enc_delta += pgm_read_byte(&table[last]);
	
	//Button-Auswertung
	switch(buttonState)
	{
		case ButtonState_Unpressed:
			if (BUTTONPRESSED) buttonState = ButtonState_Pressed;
			break;
		
		case ButtonState_Pressed:
				buttonState = ButtonState_Hold;
			break;
		
		case ButtonState_Hold:
			iButtonPressedCounter ++;
			if (iButtonPressedCounter >= BUTTON_DEBOUNCE_CYCLES && (!BUTTONPRESSED)) 
			{
				buttonState = ButtonState_Released;
				if (buttonPressed != ButtonPressed_Long) buttonPressed = ButtonPressed_Short;
			}				
			if (iButtonPressedCounter >= BUTTON_PRESSEDLONG_CYCLES)
			{
				buttonPressed = ButtonPressed_Long;
			}
			break;
		
		case ButtonState_Released:
			iButtonPressedCounter = 0;
			buttonState = ButtonState_Unpressed;
			break;
	}
}
void encoder_init( void )            //Timer 0 initialisieren
{
  TCCR0B = (1<<CS01) | (1<<CS00);	//Timer aktivieren, Prescaler 64 --> ISR wird alle 1 / (16MHz / 64 * 256) = 1.024ms aufgerufen
  TIMSK0 |= 1<<TOIE0;				//Interrupt aktivieren
  
  DDRD &= ~((1<<PD2) | (1<PD3) | (1<<PD4));	//Define Encoder Inputs
  PORTD |= (1<<PD2) | (1<<PD3) | (1<<PD4);
  sei();
}

int8_t encoder_read( void )         // Funktion zum Auslesen des Encoders
{
  int8_t val;
  cli();
  val = enc_delta;
  enc_delta = 0;
  sei();
  return val;
}

EButtonPressedState encoder_GetButtonState()
{	
	EButtonPressedState retVal = buttonPressed;
	buttonPressed = ButtonPressed_Unpressed;
	return retVal;
}
