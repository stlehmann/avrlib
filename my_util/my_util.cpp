/************************************************************************/
/* My utility library                                                   */
/* Stefan Lehmann                                                       */
/*                                                                      */
/* This is a collection of useful functions for avr programming         */
/************************************************************************/

#include "my_util.h"

/************************************************************************/
/* Delay in milliseconds, no Maximum limits                             */
/************************************************************************/
void delay(uint16_t ms)
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms(1);
	}
}

/************************************************************************/
/* Swap Bits to positions defined by a mask                             */
/*                                                                      */
/* Example:                                                             */
/* To swap the order from MSB to LSB use the following bitmask          */
/* bitmask = {7, 6, 5, 4, 3, 2, 1, 0}                                   */
/* Bit 0 --> Bit 7											            */
/* Bit 1 --> Bit 6												        */
/* Bit 2 --> Bit 5												        */
/* Bit 3 --> Bit 4												        */
/* Bit 4 --> Bit 3												        */
/* Bit 5 --> Bit 2												        */
/* Bit 6 --> Bit 1												        */
/* Bit 7 --> Bit 0												        */
/************************************************************************/
char swap(char byte_in, char bitmask[])
{
	char out=0x00;
	for (int i=0; i<8; i++)
	{
		if (bitmask[i] < 8)
		{
			char masked = (byte_in>>i) & 0x01;
			out |= (masked << (bitmask[i]));	
		}
	}
	return out;
}