#ifndef _MY_UTIL_H
#define _MY_UTIL_H   1

#include <util/delay.h>

#ifndef F_CPU
	#define	F_CPU 16000000UL
#endif

#define wait1us	_delay_loop_1((F_CPU * 0.000001) / 3)	/**< 1 us delay */
#define wait1ms	_delay_loop_2((F_CPU * 0.001) / 4)	/**< 1 ms delay */

/*Delay in milliseconds, no Maximum limits*/
void delay(uint16_t ms);

/*Swap Bits to positions defined by a mask*/
char swap(char byte_in, char bitmask[]);

#endif