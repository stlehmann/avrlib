/*
 * uart.h
 *
 * Created: 10.06.2014 13:17:11
 *  Author: lehmann
 */ 

#ifndef UART_H_
#define UART_H_


void uart_init(void);

void uart_putc(unsigned char c);

void uart_puts (char *s);

void uart_startlogging();

void uart_stoplogging();

void uart_logvalue(char* name, char* value);

#endif /* UART_H_ */