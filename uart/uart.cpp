/*
 * uart.cpp
 *
 * Created: 10.06.2014 13:15:48
 *  Author: lehmann
 */ 
#ifndef F_CPU
#define	F_CPU 16000000UL
#endif

#define BAUD 9600UL								// Baudrate
#define UBRR_VAL ((F_CPU+BAUD*8)/(BAUD*16)-1)   // clever runden
#define BAUD_REAL (F_CPU/(16*(UBRR_VAL+1)))     // Reale Baudrate
#define BAUD_ERROR ((BAUD_REAL*1000)/BAUD)		// Fehler in Promille, 1000 = kein Fehler.
#if ((BAUD_ERROR<990) || (BAUD_ERROR>1010))
#error Systematischer Fehler der Baudrate gr�sser 1% und damit zu hoch!
#endif

#include <avr/io.h>
#include <util/setbaud.h>

void uart_init(void)
{
	UBRR0 = UBRR_VAL;
	UCSR0B |= (1<<TXEN0);				// 
	UCSR0C = (1<<UCSZ01)|(1<<UCSZ00);	// Frame Format: Asynchron 8N1
}

void uart_putc(unsigned char c)
{
	while (!(UCSR0A & (1<<UDRE0)))		// wait until ready
	{}
	UDR0 = c;							// sending
}

void uart_puts (char *s)
{
	while (*s)
	{   
		uart_putc(*s);
		s++;
	}
}

//Start logging session
void uart_startlogging()
{
	uart_putc(1);
}

//End logging session
void uart_stoplogging()
{
	uart_putc(2);
}

//Log value
void uart_logvalue(char* name, char* value)
{
	uart_puts(name);
	uart_putc(':');
	uart_puts(value);
	uart_putc(';');
}