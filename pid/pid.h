/*************************************************************************
 Title	:   C++ include file for PID controller
 Author:    Stefan lehmann
 File:	    pid.h
 Software:  AVR-GCC 4.3.3
 Hardware:  any AVR device
***************************************************************************/

#ifndef pid_h_
#define pid_h_

/*! \defgroup PIDController
 */

/** \addtogroup PIDController
 * @{
 */

#include <limits.h>

/** 
 *  @name  Definitions for PID controller configuration
 *  Change these definitions to adapt basic controller behaviour
 */
#ifndef CYCLETIME
	//! interval between two calls of the `process()` function
	#define CYCLETIME 100 //cycletime in ms
#endif
//! Maximum PID controller output value
#define MAX_Y			10000	
//! Minimum PID controller output value
#define MIN_Y			0
//! Maximum value of the i term (default MAX_Y)
#define MAX_I_TERM      MAX_Y
//! Scalefactor for `Kp`
/*!
 *	The p-part of the controller output is calculated like
 *	
 *	`p = kp * delta / SCALE_P`
 */
#define SCALE_P 10
//! Scalefactor for `Ki` 
/*! The i-part of the controller output is calculated like
 *
 *  `i = ki * delta_sum / FQ / SCALE_I`
 */
#define SCALE_I 1000
//! Scalefactor for `Kd` 
/*! The d-part of the controller output is calculated like
 *
 *	`d = kd * (delta - last_delta) * FQ / SCALE_D`
 */
#define SCALE_D 10

/** 
 *  @name  Helper macros
 *  Don' t change these
 */
//! Frequency of calling the `process` function
#define FQ (1000 / CYCLETIME)
#if SCALE_I * FQ * MAX_I_TERM > LONG_MAX
	#error Value for max_deltasum is too high for 32 bit integer.
#endif 

/*! \brief An encapsulated implementation of a PID controller.
 *			
 * The PID controller consists of a proportional, an integral and a
 * differential part.
 *
 * ## Features
 *  * fixed decimals
 *  * scaled controller parameters `Kp`, `Ki` and `Kd`
 *  * antwindup
 *  * overflow prevention at compile time
 */
 
class PIDController
{
	private:
		
		int16_t p_part, i_part, d_part;	
		
		//! Maximum allowed error, avoid overflow
		int16_t max_delta;		
		//! Maximum allowed sumerror, avoid positive overflow		
		int32_t max_deltasum;
		//! Minimum allowed sumerror, avoid negative overflow
		int32_t min_deltasum;
		
	public:
		//! setpoint
		int16_t w;
		//! current value
		int16_t x;
		//! difference between setpoint and current value
		int16_t delta;
		//! cummulated difference between setpoint and current value
		int32_t delta_sum;
		//! difference between setpoint and current value of last cycle
		int16_t last_delta;
		//! controller output
		int16_t y;
		//! proportional gain
		int16_t kp;
		//! integral gain
		int16_t ki;
		//! differential gain
		int16_t kd;
		
		//!antiwindup option. 
		/*! i part will stop counting if ymax is reached
			default: true
		 */ 
		bool antiwindup;
		
		PIDController(int16_t kp, int16_t ki, int16_t kd);
		
		//! Calculate controller output.
		/*!
		 *	Call this function with a certain cycletime defined by the
		 *  `CYCLETIME` constant.
		 */
		void process();
};

/** @}*/

#endif